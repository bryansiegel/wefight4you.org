# Filters added to this controller apply to all controllers in the application.
# Likewise, all the methods added will be available for all controllers.

class ApplicationController < ActionController::Base
  helper :all # include all helpers, all the time
  protect_from_forgery # See ActionController::RequestForgeryProtection for details

  # Scrub sensitive parameters from your log
   filter_parameter_logging :password
   helper_method :admin?
  private
  
  def authorize_access
    if not session[:user_id]
      flash[:notice] = "Please Log In."
      redirect_to(:action => 'login', :controller => 'user')
      return false
  end
end


def admin?
session[:user_id] == "bryan" ||   session[:user_id] == "marcgr" || session[:user_id] == "lisaro"
end

def current_user
    @_current_user ||= session[:user_id] && User.find(session[:user_id])
  end

end
