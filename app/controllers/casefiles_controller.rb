class CasefilesController < ApplicationController
helper_method :sort_column, :sort_direction

before_filter :authorize_access, :except => [:login, :authenticate]
  def index
     #Case_type sort-------------------------------------------------------
     #@filters = Casefile::FILTERS
      #if params[:show] && @filters.collect{|f| f[:scope]}.include?(params[:show])
        #@casefiles = Casefile.send(params[:show])
      #else
        #@casefiles = Casefile.all
      #end  
      #respond_to do |format|
        #format.html # index.html.erb
        #format.xml  { render :xml => @casefiles }
      #end
    #end

    #-----table title sort----------
   
    @casefiles = Casefile.all :order => (sort_column + " " +  sort_direction)
  end

  # GET /casefiles/1
  # GET /casefiles/1.xml
  def show
    @casefile = Casefile.find(params[:id])
    
    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @casefile }
    end
  end

  # GET /casefiles/new
  # GET /casefiles/new.xml
  def new
    @casefile = Casefile.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @casefile }
    end
  end

  # GET /casefiles/1/edit
  def edit
    @casefile = Casefile.find(params[:id])
  end

  # POST /casefiles
  # POST /casefiles.xml
  def create
    @casefile = Casefile.new(params[:casefile])

    respond_to do |format|
      if @casefile.save
        format.html { redirect_to(@casefile, :notice => 'Casefile was successfully created.') }
        format.xml  { render :xml => @casefile, :status => :created, :location => @casefile }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @casefile.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /casefiles/1
  # PUT /casefiles/1.xml
  def update
    @casefile = Casefile.find(params[:id])

    respond_to do |format|
      if @casefile.update_attributes(params[:casefile])
        format.html { redirect_to(@casefile, :notice => 'Casefile was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @casefile.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /casefiles/1/1
  # DELETE /casefiles/1.xml
  def destroy
    @casefile = Casefile.find(params[:id])
    @casefile.destroy

    respond_to do |format|
      format.html { redirect_to(casefiles_url) }
      format.xml  { head :ok }
    end
  end
  
  def active
  @casefile = Casefile.all(params[:id])

end

def archive
  @casefiles = Casefile.all :order => (sort_column + " " +  sort_direction)
end


  private
  
  def sort_column
    params[:sort] || "last_name"
    
  end
  
  def sort_direction
    params[:direction] || "asc"
    
  end

end
