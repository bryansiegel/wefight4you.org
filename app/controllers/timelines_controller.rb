class TimelinesController < ApplicationController
 
  def index
    @casefile = Casefile.find(params[:casefile_id])
    @timelines = @casefile.timelines
    end

  def show
    @timeline = Timeline.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @timeline }
    end
  end

  def new
    @casefile = Casefile.find(params[:casefile_id])
     @timelines = @casefile.timelines.build 
   end

  def edit
    @casefile = Casefile.find(params[:casefile_id])
    @timeline = @casefile.timelines.find(params[:id])
  end

  def create
    @casefile = Casefile.find(params[:casefile_id])
    @timelines = @casefile.timelines.build(params[:timeline])
    
      if @timelines.save
        redirect_to casefile_timelines_url
      else
        render :action => "new" 
      end
    end

  def update
    @casefile = Casefile.find(params[:casefile_id])
    @timelines = Timeline.find(params[:id])

      if @timelines.update_attributes(params[:timeline])
         redirect_to casefile_timelines_url
      else
        render :action => "edit"
      end
    end

  def destroy
    @casefile = Casefile.find(params[:casefile_id])
    @timeline = Timeline.find(params[:id])
    @timeline.destroy

    respond_to do |format|
      format.html { redirect_to(casefile_timelines_url(@casefile)) }
      format.xml  { head :ok }
    end
  end
  
  def list
     #@casefile = Casefile.find(params[:casefile_id])
      #@timelines = @casefile.timelines
      
    @casefile = Casefile.find(:all)
    @timelines = Timeline.find(:all)
    #@timeines = @casefile.timelines
    #@timelines = Timeline.find(:all)
  end

end
