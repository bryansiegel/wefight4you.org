class LegendsController < ApplicationController
  before_filter :authorize_access, :except => [:login, :authenticate]

  def index
    @legends = Legend.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @legends }
    end
  end

  # GET /legends/1
  # GET /legends/1.xml
  def show
    @legend = Legend.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @legend }
    end
  end

  # GET /legends/new
  # GET /legends/new.xml
  def new
    @legend = Legend.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @legend }
    end
  end

  # GET /legends/1/edit
  def edit
    @legend = Legend.find(params[:id])
  end

  # POST /legends
  # POST /legends.xml
  def create
    @legend = Legend.new(params[:legend])

    respond_to do |format|
      if @legend.save
        format.html { redirect_to(@legend, :notice => 'Legend was successfully created.') }
        format.xml  { render :xml => @legend, :status => :created, :location => @legend }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @legend.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /legends/1
  # PUT /legends/1.xml
  def update
    @legend = Legend.find(params[:id])

    respond_to do |format|
      if @legend.update_attributes(params[:legend])
        format.html { redirect_to(@legend, :notice => 'Legend was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @legend.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /legends/1
  # DELETE /legends/1.xml
  def destroy
    @legend = Legend.find(params[:id])
    @legend.destroy

    respond_to do |format|
      format.html { redirect_to(legends_url) }
      format.xml  { head :ok }
    end
  end
end
