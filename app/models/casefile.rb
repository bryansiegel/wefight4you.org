class Casefile < ActiveRecord::Base
  has_many :User
  has_many :timelines
  
  #named_scope(:all)
  #named_scope (:civil, :conditions => ({:case_type => ["AL", "BF", "CI", "CV", "K", "DV", "EO", "FEHA", "HM", "HO", "IM", "IT", "JE", "LM", "MM", "PI", "RE", "SE", "WT", "SS"]}))
  #named_scope (:workerscomp, :conditions => {:case_type => "WC"})
  #named_scope (:criminal, :conditions => ({:case_type => ["CR", "CA"]}))
  #named_scope (:familylaw, :conditions => {:case_type => "FL"})
  
  #FILTERS = [
    #{:scope => "all", :label => "All"},
    #{:scope => "civil", :label => "Civil"},
    #{:scope => "workerscomp", :label => "Workers Comp"},
    #{:scope => "criminal", :label => "Criminal"},
    #{:scope => "familylaw", :label => "Family Law"}
    #]
    
end

