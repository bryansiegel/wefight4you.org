module ApplicationHelper

  def sortable(column, title = nil)
    title ||= column.titleize
    direction = column == sort_column && sort_direction == "asc" ? "desc" : "asc"
    link_to title, :sort => column, :direction => direction
  end

  #------------Dropdown Sort Filters (Casefiles)-------------

     #def select_tag_for_filter(model, nvpairs, params)
    #options = {:query => params[:query] }
    #_url = url_for(eval("#{model}_url(options)"))
    #_html = %{<label for="show">Select Case Type:</label> }
    #_html << %{<select name="show" id="show"}
    #_html << %{onchange="window.location='#{_url}' + '?show=' + this.value">}
    #nvpairs.each do |pair|
        #_html << %{<option value="#{pair[:scope]}"}
        #if params[:show] == pair[:scope] || ((params[:show].nil? || 
    #params[:show].empty?) && pair[:scope] == "all")
          #_html << %{ selected="selected"}
        #end
        #_html << %{>#{pair[:label]}}
        #_html << %{</option>}
      #end
      #_html << %{</select>}
  #end

end
