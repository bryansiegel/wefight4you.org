class CreateTimelines < ActiveRecord::Migration
  def self.up
    create_table :timelines do |t|
      t.datetime :start_date
      t.datetime :end_date
      t.text :anticipated_activity
      t.text :acutal_activity
      t.references :casefile

      t.timestamps
    end
  end

  def self.down
    drop_table :timelines
  end
end
