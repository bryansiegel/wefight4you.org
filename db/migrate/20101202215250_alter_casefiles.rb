class AlterCasefiles < ActiveRecord::Migration
  def self.up
    add_column("casefiles", "active", :boolean, :default => true)
    
    
  end

  def self.down
  remove_column("casefiles", "active")
  end
end
