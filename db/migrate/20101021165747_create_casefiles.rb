class CreateCasefiles < ActiveRecord::Migration
  def self.up
    create_table :casefiles do |t|
      t.string :case_id
      t.string :case_number
      t.string :case_type
      t.string :first_name
      t.string :last_name
      t.text :case_status
      t.text :case_actions
      t.string :attorney
      t.datetime :created_at
      t.datetime :updated_at

      t.timestamps
    end
  end

  def self.down
    drop_table :casefiles
  end
end
