class AddFeesToCasefiles < ActiveRecord::Migration
  def self.up
    add_column :casefiles, :fee, :string
  end

  def self.down
    remove_column :casefiles, :fee
  end
end
