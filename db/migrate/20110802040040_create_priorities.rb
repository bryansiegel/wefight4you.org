class CreatePriorities < ActiveRecord::Migration
  def self.up
    add_column :casefiles, :priority, :string
    end

  def self.down
    remove_column :casefiles, :priority
  end
end
