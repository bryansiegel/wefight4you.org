# Be sure to restart your server when you modify this file.

# Your secret key for verifying cookie session data integrity.
# If you change this key, all old sessions will become invalid!
# Make sure the secret is at least 30 characters and all random, 
# no regular words or you'll be exposed to dictionary attacks.
ActionController::Base.session = {
  :key         => '_wefight4youFINAL_session',
  :secret      => 'e00c981b17a1ec027d347ef520a2411ba89f6c856b470679d10bc93411ec6744096ee0445a934c304dd9e2987538e7821378b335e6ff029875681da7d94950ac'
}

# Use the database for sessions instead of the cookie-based default,
# which shouldn't be used to store highly confidential information
# (create the session table with "rake db:sessions:create")
# ActionController::Base.session_store = :active_record_store
