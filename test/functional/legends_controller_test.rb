require 'test_helper'

class LegendsControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:legends)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create legend" do
    assert_difference('Legend.count') do
      post :create, :legend => { }
    end

    assert_redirected_to legend_path(assigns(:legend))
  end

  test "should show legend" do
    get :show, :id => legends(:one).to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => legends(:one).to_param
    assert_response :success
  end

  test "should update legend" do
    put :update, :id => legends(:one).to_param, :legend => { }
    assert_redirected_to legend_path(assigns(:legend))
  end

  test "should destroy legend" do
    assert_difference('Legend.count', -1) do
      delete :destroy, :id => legends(:one).to_param
    end

    assert_redirected_to legends_path
  end
end
